import React from 'react';
import shouldPureComponentUpdate from 'react-pure-render/function';
import OperatorContainer from './containers/OperatorContainer';

@OperatorContainer
export default class Operator extends React.Component{
  static propTypes = {
    name: React.PropTypes.string.isRequired,
    children: React.PropTypes.oneOfType([React.PropTypes.array, React.PropTypes.element])
  }

  shouldComponentUpdate = shouldPureComponentUpdate;

  render() {
    return (
      <div className={`rule--operator rule--operator--${this.props.name.toUpperCase()}`}>{this.props.children}</div>
    );
  }
}
