import React from 'react';
import Immutable from 'immutable';
import Item from '../components/Item';

export default class Builder extends React.Component{
  static propTypes = {
    tree: React.PropTypes.instanceOf(Immutable.Map).isRequired,
    config: React.PropTypes.object.isRequired
  }

  render() {
    const id = this.props.tree.get('id');

    return (
      <Item key={id}
        id={id}
        path={Immutable.List.of(id)}
        type={this.props.tree.get('type')}
        properties={this.props.tree.get('properties')}
        config={this.props.config}
        actions={this.props.actions}>
        {this.props.tree.get('children')}
      </Item>
    );
  }
}
