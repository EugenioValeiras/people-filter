import React from 'react';
import map from 'lodash/collection/map';
import ReactDOM from 'react-dom';

export default class Select extends React.Component{
  static propTypes = {
    setValue: React.PropTypes.func.isRequired,
    delta: React.PropTypes.number.isRequired
  }

  handleChange() {
    const node = ReactDOM.findDOMNode(this.refs.select);
    this.props.setValue(node.value);
  }

  render() {
    const fieldDefinition = this.props.config.fields[this.props.field];
    const options = map(fieldDefinition.options, (label, value) =>
      <option key={value} value={value}>{label}</option>
    );

    return (
      <select autoFocus={this.props.delta === 0} ref="select" value={this.props.value} onChange={this.handleChange.bind(this)}>
        <option value="">Select one...</option>
        {options}
      </select>
    );
  }
}
