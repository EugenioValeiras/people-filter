import React from 'react';
import ReactDOM from 'react-dom';

export default class Number extends React.Component{
  static propTypes = {
    setValue: React.PropTypes.func.isRequired,
    delta: React.PropTypes.number.isRequired
  }

  handleChange() {
    let node = ReactDOM.findDOMNode(this.refs.text);
    this.props.setValue(node.value);
  }

  render() {
    return (
      <input autoFocus={this.props.delta === 0} type="number" ref="text" min="0" value={this.props.value} onChange={this.handleChange.bind(this)} />
    );
  }
}
