import React from 'react';
import map from 'lodash/collection/map';
import ReactDOM from 'react-dom';

export default class CountySelect extends React.Component{
  static propTypes = {
    setValue: React.PropTypes.func.isRequired,
    delta: React.PropTypes.number.isRequired
  }

  constructor(props, context) {
    super(props, context);

    this.state = {selectedState: null};
  }

  handleChange() {
    const node = ReactDOM.findDOMNode(this.refs.select);
    this.setState({selectedState: node.value})
  }

  handleChangeState() {
    const node = ReactDOM.findDOMNode(this.refs.selectState);
    this.props.setValue(node.value);
  }

  render() {
    const fieldDefinition = this.props.config.fields[this.props.field];
    const stateOptions = map(fieldDefinition.stateOptions, (label, value) =>
        <option key={value} value={label}>{label}</option>
    );

    let countyOptions;
    if (this.state.selectedState) {
      let filteredOptions = fieldDefinition.countyOptions[this.state.selectedState];
      countyOptions = map(filteredOptions, (value) =>
          <option key={value} value={value}>{value}</option>
      );
    }

    return (
        <div className="double-select">
          <select autoFocus={this.props.delta === 0} ref="select" value={this.state.selectedState} onChange={this.handleChange.bind(this)}>
            <option value="">Select one...</option>
            {stateOptions}
          </select>
          <select ref="selectState" value={this.props.value} onChange={this.handleChangeState.bind(this)} disabled={!this.state.selectedState}>
            <option value="">Select one...</option>
            {countyOptions}
          </select>
        </div>
    );
  }
}