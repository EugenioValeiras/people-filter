import React from 'react';
import ReactDOM from 'react-dom';

export default class Text extends React.Component{
  static propTypes = {
    setValue: React.PropTypes.func.isRequired,
    delta: React.PropTypes.number.isRequired
  }
  constructor(props, context) {
    super(props, context);

    this.state = {field1: null, field2: null};
  }

  handleChange() {
    let node = ReactDOM.findDOMNode(this.refs.text);
    this.props.setValue(node.value);
  }

  handleChange1() {
    let node = ReactDOM.findDOMNode(this.refs.text1);
    this.setState({field1: node.value});
    this.updateValue(node.value, this.state.field2);
  }

  handleChange2() {
    let node = ReactDOM.findDOMNode(this.refs.text2);
    console.log(node.value);
    this.setState({field2: node.value});
    this.updateValue(this.state.field1, node.value );
  }

  updateValue(field1, field2) {
    let value = '';

    if (field1 && field2) {
        value = field1.concat(' and ', field2);
    } else {
        value = field1 ? field1 : field2;
    }
    this.props.setValue(value);
  }

  _renderInputs() {
    if (this.props.operator === 'inBetween') {
     return(
         <div className="double-select">
           <input autoFocus={this.props.delta === 0} type="text" ref="text1" value={this.state.field1} onChange={this.handleChange1.bind(this)} />
           <label>&nbsp;and&nbsp;</label>
           <input autoFocus={this.props.delta === 0} type="text" ref="text2" value={this.state.field2} onChange={this.handleChange2.bind(this)} />
        </div>);
    } else {
      return(
          <input autoFocus={this.props.delta === 0} type="text" ref="text" value={this.props.value} onChange={this.handleChange.bind(this)} />
      )
    }
  }

  render() {
    return (
      this._renderInputs()
    );
  }
}
