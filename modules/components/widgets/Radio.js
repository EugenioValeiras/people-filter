import React from 'react';
import map from 'lodash/collection/map';
import ReactDOM from 'react-dom'
import uuid from '../../utils/uuid'

export default class Radio extends React.Component{
  static propTypes = {
    setValue: React.PropTypes.func.isRequired,
    delta: React.PropTypes.number.isRequired
  }

  constructor(props) {
    super(props);
    const fieldDefinition = this.props.config.fields[this.props.field];
    this.state = {selected: fieldDefinition.defaultOption};
  }

  componentDidMount() {
    const fieldDefinition = this.props.config.fields[this.props.field];
    this.props.setValue(fieldDefinition.defaultOption);
  }

  handleChange(e) {
    console.log(this.props);
    this.setState({selected: e.currentTarget.value});
    this.props.setValue(e.currentTarget.value);
  }

  render() {
    var self = this;
    const fieldName = uuid();
    const fieldDefinition = this.props.config.fields[this.props.field];
    const options = map(fieldDefinition.options, (label, value) => {
        return (
            <div key={value} className="checkbox">
              <label>
                <input value={value} type="radio" name={fieldName} checked={self.state.selected === value} onChange={this.handleChange.bind(this)}/>{label}
              </label>
            </div>
        )
    });

    return (
        <div>{options}</div>
    );
  }
}
