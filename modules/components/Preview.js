import React from 'react';
import shouldPureComponentUpdate from 'react-pure-render/function';
import queryString from '../utils/queryString';

export default class Preview extends React.Component{
  static propTypes = {
    config: React.PropTypes.object.isRequired
  }

  shouldComponentUpdate = shouldPureComponentUpdate;

  render() {
    return this.props.children(queryString(this.props.tree, this.props.config));
  }
}
