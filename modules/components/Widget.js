import React from 'react';
import shouldPureComponentUpdate from 'react-pure-render/function';
import WidgetContainer from './containers/WidgetContainer';

@WidgetContainer
export default class Widget extends React.Component{
  static propTypes = {
    name: React.PropTypes.string.isRequired,
    children: React.PropTypes.oneOfType([React.PropTypes.array, React.PropTypes.element])
  }

  shouldComponentUpdate = shouldPureComponentUpdate;

  render() {
    return (
      <div className={`rule--widget rule--widget--${this.props.name.toUpperCase()}`}>{this.props.children}</div>
    );
  }
}
