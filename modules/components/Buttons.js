import React from 'react';
import Button from 'react-bootstrap/lib/Button';
import queryString from '../utils/queryString';

export default class Buttons extends React.Component{
    static propTypes: {
        onSave: React.PropTypes.func.isRequired
    }

    _save() {
        var string = queryString(this.props.tree, this.props.config);
        this.props.onSave(string);
    }

    render() {
        return (
         <div className="buttons-menu">
           <button onClick={this.props.actions.resetTree} className="btn btn-danger" type="button">Reset</button>
           <button onClick={this._save.bind(this)} className="btn btn-success" type="button">Save</button>
         </div>
        );
    }
}
