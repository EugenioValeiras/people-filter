import React from 'react';
import createTreeStore from '../stores/tree';
import { createStore } from 'redux';
import { Provider, Connector } from 'redux/react';
import bindActionCreators from '../utils/bindActionCreators';
import BigOverLay from '../utils/BigOverLay';
import * as actions from '../actions';

export default class Query extends React.Component{
  static propTypes: {
    conjunctions: React.PropTypes.object.isRequired,
    fields: React.PropTypes.object.isRequired,
    operators: React.PropTypes.object.isRequired,
    widgets: React.PropTypes.object.isRequired,
    settings: React.PropTypes.object.isRequired
  }

  constructor(props, context) {
    super(props, context);

    const config = {
      conjunctions: props.conjunctions,
      fields: props.fields,
      operators: props.operators,
      widgets: props.widgets,
      settings: props.settings
    };

    const tree = createTreeStore(config);

    this.state = {
      store: createStore({ tree })
    };
  }

  render() {
    const { conjunctions, fields, operators, widgets, settings, children, show, onHide, ...props } = this.props;
    const config = { conjunctions, fields, operators, widgets, settings };

    return (
       <BigOverLay show={show} onHide={onHide} header="Advanced People Filter">
            <Provider store={this.state.store}>{() => (
              <Connector select={({ tree }) => ({ tree })}>
                {({ tree, dispatch }) => {
                  return children({
                    tree: tree,
                    actions: bindActionCreators({ ...actions.tree, ...actions.group, ...actions.rule }, config, dispatch),
                    config: config
                  });
                }}
              </Connector>
            )}</Provider>
        </BigOverLay>
    );
  }
}
