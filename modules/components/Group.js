import React from 'react';
import shouldPureComponentUpdate from 'react-pure-render/function';
import map from 'lodash/collection/map';
import GroupContainer from './containers/GroupContainer';

@GroupContainer
export default class Group extends React.Component{
  static propTypes = {
    conjunctionOptions: React.PropTypes.object.isRequired,
    addRule: React.PropTypes.func.isRequired,
    addGroup: React.PropTypes.func.isRequired,
    removeSelf: React.PropTypes.func.isRequired,
    allowFurtherNesting: React.PropTypes.bool.isRequired,
    allowRemoval: React.PropTypes.bool.isRequired
  }

  shouldComponentUpdate = shouldPureComponentUpdate;

  render() {
    return (
      <div className="group">
        <div className="group--header">
          <div className="group--conjunctions">
            {map(this.props.conjunctionOptions, (item, index) => (
              <div key={index} className={`conjunction conjunction--${index.toUpperCase()}`} data-state={item.checked ? 'active' : 'inactive'}>
                <label htmlFor={item.id}>{item.label}</label>
                <input id={item.id} type="radio"name={item.name} value={index} checked={item.checked} onChange={item.setConjunction} />
              </div>
            ))}
          </div>
          <div className="group--actions">
            <button className="action action--ADD-RULE" onClick={this.props.addRule}>+ Add rule</button>
            {this.props.allowFurtherNesting ? (
              <button className="action action--ADD-GROUP" onClick={this.props.addGroup}>+ Add group</button>
            ) : null}
            {this.props.allowRemoval ? (
              <button className="action action--DELETE" onClick={this.props.removeSelf}>Delete</button>
            ) : null}
          </div>
        </div>
        {this.props.children ? (
          <div className="group--children">{this.props.children}</div>
        ) : null}
      </div>
    );
  }
}
