import React from 'react';
import shouldPureComponentUpdate from 'react-pure-render/function';
import map from 'lodash/collection/map';
import size from 'lodash/collection/size';
import RuleContainer from './containers/RuleContainer';
import ReactDOM from 'react-dom';

@RuleContainer
export default class Rule extends React.Component{
  static propTypes = {
    fieldOptions: React.PropTypes.object.isRequired,
    operatorOptions: React.PropTypes.object.isRequired,
    setField: React.PropTypes.func.isRequired,
    setOperator: React.PropTypes.func.isRequired,
    removeSelf: React.PropTypes.func.isRequired,
    selectedField: React.PropTypes.string,
    selectedOperator: React.PropTypes.string
  }

  shouldComponentUpdate = shouldPureComponentUpdate;

  handleFieldSelect() {
    const node = ReactDOM.findDOMNode(this.refs.field);
    this.props.setField(node.value);
  }

  handleOperatorSelect() {
    const node = ReactDOM.findDOMNode(this.refs.operator);
    this.props.setOperator(node.value);
  }

  render() {
    return (
      <div className="rule">
        <div className="rule--header">
          <div className="rule--actions">
            <button className="action action--DELETE" onClick={this.props.removeSelf}>Delete</button>
          </div>
        </div>
        <div className="rule--body">
          {size(this.props.fieldOptions) ? (
            <div key="field" className="rule--field">
              <select ref="field" value={this.props.selectedField} onChange={this.handleFieldSelect.bind(this)}>
                {map(this.props.fieldOptions, (label, value) => (
                  <option key={value} value={value}>{label}</option>
                ))}
              </select>
            </div>
          ) : null}
          {size(this.props.operatorOptions) ? (
            <div key="operator" className="rule--operator">
              <select ref="operator" value={this.props.selectedOperator} onChange={this.handleOperatorSelect.bind(this)}>
                {map(this.props.operatorOptions, (label, value) => (
                  <option key={value} value={value}>{label}</option>
                ))}
              </select>
            </div>
          ) : null}
          {this.props.children}
        </div>
      </div>
    );
  }
}
