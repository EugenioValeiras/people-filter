import React, { Component } from 'react';
import { PeopleFilter, Builder, Preview, Buttons } from 'react-query-builder';
import config from './config';

export default class Demo extends Component {
  saveCallBack(query) {
      console.log(query);
      alert(query);
  }

  render() {
    return (
    <PeopleFilter {...config} show >
        {(props) => (
            <div>
                <div className="query-builder">
                    <Builder {...props} />
                </div>
                <Buttons {...props} onSave={this.saveCallBack}/>
            </div>
        )}
    </PeopleFilter>
    );
  }
}
