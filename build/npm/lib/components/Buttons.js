'use strict';

var _inherits = require('babel-runtime/helpers/inherits')['default'];

var _classCallCheck = require('babel-runtime/helpers/class-call-check')['default'];

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

exports.__esModule = true;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactBootstrapLibButton = require('react-bootstrap/lib/Button');

var _reactBootstrapLibButton2 = _interopRequireDefault(_reactBootstrapLibButton);

var _utilsQueryString = require('../utils/queryString');

var _utilsQueryString2 = _interopRequireDefault(_utilsQueryString);

var Buttons = (function (_Component) {
    _inherits(Buttons, _Component);

    function Buttons() {
        _classCallCheck(this, Buttons);

        _Component.apply(this, arguments);
    }

    Buttons.prototype._save = function _save() {
        var string = _utilsQueryString2['default'](this.props.tree, this.props.config);
        this.props.onSave(string);
    };

    Buttons.prototype.render = function render() {
        return _react2['default'].createElement(
            'div',
            { className: 'buttons-menu' },
            _react2['default'].createElement(
                'button',
                { onClick: this.props.actions.resetTree, className: 'btn btn-danger', type: 'button' },
                'Reset'
            ),
            _react2['default'].createElement(
                'button',
                { onClick: this._save.bind(this), className: 'btn btn-success', type: 'button' },
                'Save'
            )
        );
    };

    return Buttons;
})(_react.Component);

exports['default'] = Buttons;
module.exports = exports['default'];