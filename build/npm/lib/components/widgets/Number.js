'use strict';

var _inherits = require('babel-runtime/helpers/inherits')['default'];

var _createClass = require('babel-runtime/helpers/create-class')['default'];

var _classCallCheck = require('babel-runtime/helpers/class-call-check')['default'];

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

exports.__esModule = true;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var Number = (function (_Component) {
  _inherits(Number, _Component);

  function Number() {
    _classCallCheck(this, Number);

    _Component.apply(this, arguments);
  }

  Number.prototype.handleChange = function handleChange() {
    var node = _reactDom2['default'].findDOMNode(this.refs.text);
    this.props.setValue(node.value);
  };

  Number.prototype.render = function render() {
    return _react2['default'].createElement('input', { autoFocus: this.props.delta === 0, type: 'number', ref: 'text', min: '0', value: this.props.value, onChange: this.handleChange.bind(this) });
  };

  _createClass(Number, null, [{
    key: 'propTypes',
    value: {
      setValue: _react.PropTypes.func.isRequired,
      delta: _react.PropTypes.number.isRequired
    },
    enumerable: true
  }]);

  return Number;
})(_react.Component);

exports['default'] = Number;
module.exports = exports['default'];