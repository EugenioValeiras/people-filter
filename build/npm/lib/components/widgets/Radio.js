'use strict';

var _inherits = require('babel-runtime/helpers/inherits')['default'];

var _createClass = require('babel-runtime/helpers/create-class')['default'];

var _classCallCheck = require('babel-runtime/helpers/class-call-check')['default'];

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

exports.__esModule = true;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _lodashCollectionMap = require('lodash/collection/map');

var _lodashCollectionMap2 = _interopRequireDefault(_lodashCollectionMap);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _utilsUuid = require('../../utils/uuid');

var _utilsUuid2 = _interopRequireDefault(_utilsUuid);

var Radio = (function (_Component) {
  _inherits(Radio, _Component);

  _createClass(Radio, null, [{
    key: 'propTypes',
    value: {
      setValue: _react.PropTypes.func.isRequired,
      delta: _react.PropTypes.number.isRequired
    },
    enumerable: true
  }]);

  function Radio(props) {
    _classCallCheck(this, Radio);

    _Component.call(this, props);
    var fieldDefinition = this.props.config.fields[this.props.field];
    this.state = { selected: fieldDefinition.defaultOption };
  }

  Radio.prototype.componentDidMount = function componentDidMount() {
    var fieldDefinition = this.props.config.fields[this.props.field];
    this.props.setValue(fieldDefinition.defaultOption);
  };

  Radio.prototype.handleChange = function handleChange(e) {
    console.log(this.props);
    this.setState({ selected: e.currentTarget.value });
    this.props.setValue(e.currentTarget.value);
  };

  Radio.prototype.render = function render() {
    var _this = this;

    var self = this;
    var fieldName = _utilsUuid2['default']();
    var fieldDefinition = this.props.config.fields[this.props.field];
    var options = _lodashCollectionMap2['default'](fieldDefinition.options, function (label, value) {
      return _react2['default'].createElement(
        'div',
        { key: value, className: 'checkbox' },
        _react2['default'].createElement(
          'label',
          null,
          _react2['default'].createElement('input', { value: value, type: 'radio', name: fieldName, checked: self.state.selected === value, onChange: _this.handleChange.bind(_this) }),
          label
        )
      );
    });

    return _react2['default'].createElement(
      'div',
      null,
      options
    );
  };

  return Radio;
})(_react.Component);

exports['default'] = Radio;
module.exports = exports['default'];