'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

var _interopRequireWildcard = require('babel-runtime/helpers/interop-require-wildcard')['default'];

exports.__esModule = true;

var _immutable = require('immutable');

var _immutable2 = _interopRequireDefault(_immutable);

var _utilsExpandTreePath = require('../utils/expandTreePath');

var _utilsExpandTreePath2 = _interopRequireDefault(_utilsExpandTreePath);

var _utilsDefaultRoot = require('../utils/defaultRoot');

var _utilsDefaultRoot2 = _interopRequireDefault(_utilsDefaultRoot);

var _utilsDefaultRuleProperties = require('../utils/defaultRuleProperties');

var _constants = require('../constants');

var constants = _interopRequireWildcard(_constants);

/**
 * @param {Immutable.Map} state
 * @param {Immutable.List} path
 * @param {string} conjunction
 */
var setConjunction = function setConjunction(state, path, conjunction) {
  return state.setIn(_utilsExpandTreePath2['default'](path, 'properties', 'conjunction'), conjunction);
};

/**
 * @param {Immutable.Map} state
 * @param {Immutable.List} path
 * @param {string} type
 * @param {string} id
 * @param {Immutable.OrderedMap} properties
 */
var addItem = function addItem(state, path, type, id, properties) {
  var _ref;

  return state.mergeIn(_utilsExpandTreePath2['default'](path, 'children'), new _immutable2['default'].OrderedMap((_ref = {}, _ref[id] = new _immutable2['default'].Map({ type: type, id: id, properties: properties }), _ref)));
};

/**
 * @param {Immutable.Map} state
 * @param {Immutable.List} path
 */
var removeItem = function removeItem(state, path) {
  return state.deleteIn(_utilsExpandTreePath2['default'](path));
};

/**
 * @param {Immutable.Map} state
 * @param {Immutable.List} path
 * @param {string} field
 */
var setField = function setField(state, path, field, config) {
  return state.updateIn(_utilsExpandTreePath2['default'](path, 'properties'), function (map) {
    return map.withMutations(function (current) {
      var currentField = current.get('field');
      var currentOperator = current.get('operator');
      var currentValue = current.get('value');

      // If the newly selected field supports the same operator the rule currently
      // uses, keep it selected.
      var operator = config.fields[field].operators.indexOf(currentOperator) !== -1 ? currentOperator : _utilsDefaultRuleProperties.defaultOperator(config, field);

      var operatorCardinality = config.operators[operator].cardinality || 1;

      return current.set('field', field).set('operator', operator).set('operatorOptions', _utilsDefaultRuleProperties.defaultOperatorOptions(config, operator)).set('valueOptions', _utilsDefaultRuleProperties.defaultValueOptions(config, operator)).set('value', (function (currentWidget, nextWidget) {
        return currentWidget !== nextWidget ? new _immutable2['default'].List() : new _immutable2['default'].List(currentValue.take(operatorCardinality));
      })(config.fields[currentField].widget, config.fields[field].widget));
    });
  });
};

/**
 * @param {Immutable.Map} state
 * @param {Immutable.List} path
 * @param {string} operator
 */
var setOperator = function setOperator(state, path, operator, config) {
  return state.updateIn(_utilsExpandTreePath2['default'](path, 'properties'), function (map) {
    return map.withMutations(function (current) {
      var operatorCardinality = config.operators[operator].cardinality || 1;
      var currentValue = current.get('value', new _immutable2['default'].List());
      var nextValue = new _immutable2['default'].List(currentValue.take(operatorCardinality));

      return current.set('operator', operator).set('operatorOptions', _utilsDefaultRuleProperties.defaultOperatorOptions(config, operator)).set('valueOptions', _utilsDefaultRuleProperties.defaultValueOptions(config, operator)).set('value', nextValue);
    });
  });
};

/**
 * @param {Immutable.Map} state
 * @param {Immutable.List} path
 * @param {integer} delta
 * @param {*} value
 */
var setValue = function setValue(state, path, delta, value) {
  return state.setIn(_utilsExpandTreePath2['default'](path, 'properties', 'value', delta + ''), value);
};

/**
 * @param {Immutable.Map} state
 * @param {Immutable.List} path
 * @param {string} name
 * @param {*} value
 */
var setOperatorOption = function setOperatorOption(state, path, name, value) {
  return state.setIn(_utilsExpandTreePath2['default'](path, 'properties', 'operatorOptions', name), value);
};

/**
 * @param {Immutable.Map} state
 * @param {Immutable.List} path
 * @param {string} name
 * @param {*} value
 */
var setValueOption = function setValueOption(state, path, delta, name, value) {
  return state.setIn(_utilsExpandTreePath2['default'](path, 'properties', 'valueOptions', delta + '', name), value);
};

/**
 * @param {Immutable.Map} state
 * @param {object} action
 */

exports['default'] = function (config) {
  return function (state, action) {
    if (state === undefined) state = _utilsDefaultRoot2['default'](config);

    switch (action.type) {
      case constants.RESET_TREE:
        return _utilsDefaultRoot2['default'](config);
      case constants.SET_TREE:
        return action.tree;

      case constants.ADD_GROUP:
        return addItem(state, action.path, 'group', action.id, action.properties);

      case constants.REMOVE_GROUP:
        return removeItem(state, action.path);

      case constants.ADD_RULE:
        return addItem(state, action.path, 'rule', action.id, action.properties);

      case constants.REMOVE_RULE:
        return removeItem(state, action.path);

      case constants.SET_CONJUNCTION:
        return setConjunction(state, action.path, action.conjunction);

      case constants.SET_FIELD:
        return setField(state, action.path, action.field, action.config);

      case constants.SET_OPERATOR:
        return setOperator(state, action.path, action.operator, action.config);

      case constants.SET_VALUE:
        return setValue(state, action.path, action.delta, action.value);

      case constants.SET_OPERATOR_OPTION:
        return setOperatorOption(state, action.path, action.name, action.value);

      case constants.SET_VALUE_OPTION:
        return setValueOption(state, action.path, action.delta, action.name, action.value);

      default:
        return state;
    }
  };
};

module.exports = exports['default'];