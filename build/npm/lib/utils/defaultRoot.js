'use strict';

var _extends = require('babel-runtime/helpers/extends')['default'];

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

exports.__esModule = true;

var _immutable = require('immutable');

var _immutable2 = _interopRequireDefault(_immutable);

var _uuid = require('./uuid');

var _uuid2 = _interopRequireDefault(_uuid);

var _defaultRuleProperties = require('./defaultRuleProperties');

var _defaultRuleProperties2 = _interopRequireDefault(_defaultRuleProperties);

var _defaultGroupProperties = require('./defaultGroupProperties');

var _defaultGroupProperties2 = _interopRequireDefault(_defaultGroupProperties);

var getChild = function getChild(id, config) {
  var _ref;

  return _ref = {}, _ref[id] = new _immutable2['default'].Map({
    type: 'rule',
    id: id,
    properties: _defaultRuleProperties2['default'](config)
  }), _ref;
};

exports.getChild = getChild;

exports['default'] = function (config) {
  return new _immutable2['default'].Map({
    type: 'group',
    id: _uuid2['default'](),
    children: new _immutable2['default'].OrderedMap(_extends({}, getChild(_uuid2['default'](), config), getChild(_uuid2['default'](), config))),
    properties: _defaultGroupProperties2['default'](config)
  });
};