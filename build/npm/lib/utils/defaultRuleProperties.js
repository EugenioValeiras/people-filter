'use strict';

var _Object$keys = require('babel-runtime/core-js/object/keys')['default'];

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

exports.__esModule = true;

var _immutable = require('immutable');

var _immutable2 = _interopRequireDefault(_immutable);

var _lodashCollectionMap = require('lodash/collection/map');

var _lodashCollectionMap2 = _interopRequireDefault(_lodashCollectionMap);

var _lodashUtilityRange = require('lodash/utility/range');

var _lodashUtilityRange2 = _interopRequireDefault(_lodashUtilityRange);

var defaultField = function defaultField(config) {
  return typeof config.settings.defaultField === 'function' ? config.settings.defaultField(config) : config.settings.defaultField || _Object$keys(config.fields)[0];
};

exports.defaultField = defaultField;
var defaultOperator = function defaultOperator(config, field) {
  return typeof config.settings.defaultOperator === 'function' ? config.settings.defaultOperator(field, config) : config.settings.defaultOperator || config.fields[field].operators[0];
};

exports.defaultOperator = defaultOperator;
var defaultOperatorOptions = function defaultOperatorOptions(config, operator) {
  return new _immutable2['default'].Map(config.operators[operator].options && config.operators[operator].options.defaults || {});
};

exports.defaultOperatorOptions = defaultOperatorOptions;
var defaultValueOptions = function defaultValueOptions(config, operator) {
  return new _immutable2['default'].List(_lodashCollectionMap2['default'](_lodashUtilityRange2['default'](0, config.operators[operator].cardinality), function () {
    return new _immutable2['default'].Map(config.operators[operator].valueOptions && config.operators[operator].valueOptions.defaults || {});
  }));
};

exports.defaultValueOptions = defaultValueOptions;

exports['default'] = function (config) {
  var field = defaultField(config, field);
  var operator = defaultOperator(config, field);

  return new _immutable2['default'].Map({
    field: field,
    operator: operator,
    value: new _immutable2['default'].List(),
    operatorOptions: defaultOperatorOptions(config, operator),
    valueOptions: defaultValueOptions(config, operator)
  });
};