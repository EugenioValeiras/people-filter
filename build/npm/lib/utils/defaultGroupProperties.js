'use strict';

var _Object$keys = require('babel-runtime/core-js/object/keys')['default'];

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

exports.__esModule = true;

var _immutable = require('immutable');

var _immutable2 = _interopRequireDefault(_immutable);

var defaultConjunction = function defaultConjunction(config) {
  return config.settings.defaultConjunction || _Object$keys(config.conjunctions)[0];
};

exports.defaultConjunction = defaultConjunction;

exports['default'] = function (config) {
  return new _immutable2['default'].Map({
    conjunction: defaultConjunction(config)
  });
};