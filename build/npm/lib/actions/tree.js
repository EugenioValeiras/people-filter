'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

var _interopRequireWildcard = require('babel-runtime/helpers/interop-require-wildcard')['default'];

exports.__esModule = true;

var _utilsUuid = require('../utils/uuid');

var _utilsUuid2 = _interopRequireDefault(_utilsUuid);

var _utilsExpandTreePath = require('../utils/expandTreePath');

var _utilsExpandTreePath2 = _interopRequireDefault(_utilsExpandTreePath);

var _utilsDefaultRuleProperties = require('../utils/defaultRuleProperties');

var _utilsDefaultRuleProperties2 = _interopRequireDefault(_utilsDefaultRuleProperties);

var _utilsDefaultGroupProperties = require('../utils/defaultGroupProperties');

var _utilsDefaultGroupProperties2 = _interopRequireDefault(_utilsDefaultGroupProperties);

var _constants = require('../constants');

var constants = _interopRequireWildcard(_constants);

var hasChildren = function hasChildren(tree, path) {
  return tree.getIn(_utilsExpandTreePath2['default'](path, 'children')).size > 0;
};

/**
 * @param {object} config
 * @param {Immutable.Map} tree
 */
var resetTree = function resetTree(config, tree) {
  return {
    type: constants.RESET_TREE,
    tree: tree
  };
};

exports.resetTree = resetTree;
/**
 * @param {object} config
 * @param {Immutable.Map} tree
 */
var setTree = function setTree(config, tree) {
  return {
    type: constants.SET_TREE,
    tree: tree
  };
};

exports.setTree = setTree;
/**
 * @param {object} config
 * @param {Immutable.List} path
 * @param {object} properties
 */
var addRule = function addRule(config, path, properties) {
  return {
    type: constants.ADD_RULE,
    path: path,
    id: _utilsUuid2['default'](),
    properties: _utilsDefaultRuleProperties2['default'](config).merge(properties || {})
  };
};

exports.addRule = addRule;
/**
 * @param {object} config
 * @param {Immutable.List} path
 */
var removeRule = function removeRule(config, path) {
  return function (dispatch, getState) {
    dispatch({
      type: constants.REMOVE_RULE,
      path: path,
      config: config
    });

    var _getState = getState();

    var tree = _getState.tree;

    var parentPath = path.slice(0, -1);
    if (!hasChildren(tree, parentPath)) {
      dispatch(addRule(config, parentPath));
    }
  };
};

exports.removeRule = removeRule;
/**
 * @param {object} config
 * @param {Immutable.List} path
 * @param {object} properties
 */
var addGroup = function addGroup(config, path, properties) {
  return function (dispatch) {
    var groupUuid = _utilsUuid2['default']();

    dispatch({
      type: constants.ADD_GROUP,
      path: path,
      id: groupUuid,
      properties: _utilsDefaultGroupProperties2['default'](config).merge(properties || {}),
      config: config
    });

    var groupPath = path.push(groupUuid);
    dispatch(addRule(config, groupPath));
    dispatch(addRule(config, groupPath));
  };
};

exports.addGroup = addGroup;
/**
 * @param {object} config
 * @param {Immutable.List} path
 */
var removeGroup = function removeGroup(config, path) {
  return function (dispatch, getState) {
    dispatch({
      type: constants.REMOVE_GROUP,
      path: path,
      config: config
    });

    var _getState2 = getState();

    var tree = _getState2.tree;

    var parentPath = path.slice(0, -1);
    if (!hasChildren(tree, parentPath)) {
      dispatch(addRule(config, parentPath));
    }
  };
};
exports.removeGroup = removeGroup;