'use strict';

var _interopRequireWildcard = require('babel-runtime/helpers/interop-require-wildcard')['default'];

exports.__esModule = true;

var _constants = require('../constants');

var constants = _interopRequireWildcard(_constants);

/**
 * @param {object} config
 * @param {Immutable.List} path
 * @param {string} field
 */
var setField = function setField(config, path, field) {
  return {
    type: constants.SET_FIELD,
    path: path,
    field: field,
    config: config
  };
};

exports.setField = setField;
/**
 * @param {object} config
 * @param {Immutable.List} path
 * @param {string} operator
 */
var setOperator = function setOperator(config, path, operator) {
  return {
    type: constants.SET_OPERATOR,
    path: path,
    operator: operator,
    config: config
  };
};

exports.setOperator = setOperator;
/**
 * @param {object} config
 * @param {Immutable.List} path
 * @param {integer} delta
 * @param {*} value
 */
var setValue = function setValue(config, path, delta, value) {
  return {
    type: constants.SET_VALUE,
    path: path,
    delta: delta,
    value: value,
    config: config
  };
};

exports.setValue = setValue;
/**
 * @param {object} config
 * @param {Immutable.List} path
 * @param {string} name
 * @param {*} value
 */
var setOperatorOption = function setOperatorOption(config, path, name, value) {
  return {
    type: constants.SET_OPERATOR_OPTION,
    path: path,
    name: name,
    value: value,
    config: config
  };
};

exports.setOperatorOption = setOperatorOption;
/**
 * @param {object} config
 * @param {Immutable.List} path
 * @param {integer} delta
 * @param {string} name
 * @param {*} value
 */
var setValueOption = function setValueOption(config, path, delta, name, value) {
  return {
    type: constants.SET_VALUE_OPTION,
    path: path,
    delta: delta,
    name: name,
    value: value,
    config: config
  };
};
exports.setValueOption = setValueOption;